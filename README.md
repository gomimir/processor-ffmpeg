# Mimir FFmpeg processor

This app is a Mimir processor for video files.

For more information about how to use it see [documentation](https://gomimir.gitlab.io/processors/video).

## Developing in docker container

This repo is prepared to be developed [inside a container](https://code.visualstudio.com/docs/remote/containers).

Sample `.devcontainer/devcontainer.json` (see https://aka.ms/devcontainer.json)

```json
{
	"name": "Existing Dockerfile",

	"build": {
		"target": "dev"
	},

	"dockerFile": "../Dockerfile",

	"settings": { 
		"go.toolsManagement.checkForUpdates": "local",
		"go.useLanguageServer": true,
		"go.gopath": "/go",
		"go.goroot": "/usr/local/go"
	},
	
	"extensions": [
		"golang.Go"
	],

	// Note: replace with your host IP if you wish to test against local server
	"containerEnv": {
		"TZ": "Europe/Prague",
		"MIMIR_SERVER_HOST": "192.168.3.234:3001",
		"MIMIR_REDIS_HOST": "192.168.3.234:6379"
	},

	// For mounting processor lib
	// "mounts": [ "source=/Users/adam/Development/mimir/processor,target=/workspaces/processor,type=bind" ],
}
```

# Test run

```bash
go run cmd/processor-ffmpeg/processor.go test processVideo https://raw.githubusercontent.com/prof3ssorSt3v3/media-sample-files/master/lion-sample.mp4
```

# Processing from docker container

Note: replace with your host IP

```bash
export MIMIR_SERVER_HOST=192.168.3.234:3001
export MIMIR_REDIS_HOST=192.168.3.234:6379
make run
```
