package main

import (
	"context"
	"fmt"
	"os"
	"regexp"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	mimir "gitlab.com/gomimir/processor"
	"gitlab.com/gomimir/processor-ffmpeg/internal/probe"
	"gitlab.com/gomimir/processor-ffmpeg/internal/thumbnailer"
	"gitlab.com/gomimir/processor-ffmpeg/internal/transcoder"
	"gitlab.com/gomimir/processor-ffmpeg/internal/utils"
	mimirapp "gitlab.com/gomimir/processor/pkg/app"
	mimirpb "gitlab.com/gomimir/processor/protobuf"
	mimirpbconv "gitlab.com/gomimir/processor/protobufconv"
)

// populated by build flags
var version string

var suffixRX = regexp.MustCompile(`(\.[^./]+)?$`)

func main() {
	app := mimirapp.NewProcessorApp("mimir-processor-ffmpeg", version)
	app.SetDefaultProcessingQueue("ffmpeg")

	processVideo := app.RegisterFileProcessor("processVideo", processHandler)
	transcodeVideo := app.RegisterFileProcessor("transcodeVideo", transcodeHandler)

	// Ensure temporary directory on start
	app.RootCommand().PersistentFlags().String("temp-dir", "/tmp", "temporary directory where to download video files")
	processVideo.TestCommand().PreRun = func(cmd *cobra.Command, args []string) { utils.EnsureTempDir(viper.GetString("temp-dir")) }
	transcodeVideo.TestCommand().PreRun = func(cmd *cobra.Command, args []string) { utils.EnsureTempDir(viper.GetString("temp-dir")) }
	app.OnProcessorStart = func() { utils.EnsureTempDir(viper.GetString("temp-dir")) }

	// Redirect native logger to zerolog
	utils.SetupLoggerStub()

	app.Execute()
}

func transcodeHandler(req mimirapp.FileProcessorRequest) error {
	tf, err := utils.RetrieveTaskFile(req)
	if err != nil {
		return err
	}

	if tf.NeedsCleanup {
		defer utils.RemoveTempFile(tf.Filename)
	}

	return transcode(req, tf)
}

func transcode(req mimirapp.FileProcessorRequest, tf *utils.TaskFile) error {
	start := time.Now()

	outFilename := suffixRX.ReplaceAllString(tf.Filename, "-transcoded.mp4")

	err := transcoder.Transcode(transcoder.TranscodeReq{
		InputFilename:  tf.Filename,
		OutputFilename: outFilename,
		VideoCodec:     "libx264",
		AudioCodec:     "aac",
	})

	if err != nil {
		log.Error().Err(err).Msg("Failed to transcode the video")
		return err
	}

	duration := time.Since(start)
	log.Info().Str("took", fmt.Sprintf("%v", duration)).Msg("Transcode completed")

	defer utils.RemoveTempFile(outFilename)

	if !req.IsTest {
		md, err := probe.GetMetadata(outFilename)
		if err != nil {
			log.Error().Err(err).Msg("Failed to extract metadata from the transcoded video")
			return err
		}

		desc := mimir.VideoDescriptor{
			Format:     mimir.VF_MP4,
			Width:      md.Width,
			Height:     md.Height,
			VideoCodec: md.VideoCodec,
			AudioCodec: md.AudioCodec,
		}

		pbVideoDesc, err := mimirpbconv.VideoDescToProtoBuf(desc)
		if err != nil {
			req.Log.Error().Err(err).Msg("Failed to prepare video descriptor")
			return err
		}

		bytes, err := os.ReadFile(outFilename)
		if err != nil {
			req.Log.Error().Err(err).Msg("Failed to read the transcoded file")
			return err
		}

		return uploadAssetSidecars(req, []*mimirpb.UploadAssetSidecarRequest{
			{
				SourceFile: &mimirpb.FileRef{
					Repository: req.Task.File().FileRef().Repository,
					Filename:   req.Task.File().FileRef().Filename,
				},
				Filename: "transcoded-fullres-h264.mp4",
				Bytes:    bytes,
				SidecarDescriptor: &mimirpb.UploadAssetSidecarRequest_VideoDescriptor{
					VideoDescriptor: pbVideoDesc,
				},
			},
		})
	}

	return nil
}

func processHandler(req mimirapp.FileProcessorRequest) error {
	tf, err := utils.RetrieveTaskFile(req)
	if err != nil {
		return err
	}

	if tf.NeedsCleanup {
		defer utils.RemoveTempFile(tf.Filename)
	}

	filename := tf.Filename

	md, err := extractMetadata(req, filename)
	if err != nil {
		return err
	}

	if md.Width != 0 && md.Height != 0 {
		err = extractThumbnail(req, thumbnailer.ThumbnailReq{
			Filename: filename,
			Width:    md.Width,
			Height:   md.Height,
		})

		if err != nil {
			return err
		}
	}

	if md.VideoCodec != "h264" {
		err = transcode(req, tf)
		if err != nil {
			return err
		}
	}

	return nil
}

func extractThumbnail(req mimirapp.FileProcessorRequest, thumbReq thumbnailer.ThumbnailReq) error {
	start := time.Now()

	thumb, err := thumbnailer.GetThumbnail(thumbReq)
	if err != nil {
		log.Error().Err(err).Msg("Failed to extract thumbnail")
		return err
	}

	since := time.Since(start)
	log.Info().Str("took", fmt.Sprintf("%v", since)).Msg("Thumbnail extracted")

	if req.IsTest {
		// err = ioutil.WriteFile("out.jpg", thumb.Bytes, 0640)
		// if err != nil {
		// 	log.Error().Err(err).Msg("Unable to write thumbnail file")
		// 	return err
		// }
	} else {
		thumbDesc, err := mimirpbconv.ThumbnailDescToProtoBuf(thumb.ThumbnailDescriptor)
		if err != nil {
			req.Log.Error().Err(err).Msg("Failed to prepare thumbnail descriptor")
			return err
		}

		return uploadAssetSidecars(req, []*mimirpb.UploadAssetSidecarRequest{
			{
				SourceFile: &mimirpb.FileRef{
					Repository: req.Task.File().FileRef().Repository,
					Filename:   req.Task.File().FileRef().Filename,
				},
				Filename: "fullres.jpg",
				Bytes:    thumb.Bytes,
				SidecarDescriptor: &mimirpb.UploadAssetSidecarRequest_ThumbnailDescriptor{
					ThumbnailDescriptor: thumbDesc,
				},
			},
		})
	}

	return nil
}

func extractMetadata(req mimirapp.FileProcessorRequest, srcFilename string) (*probe.VideoMetadata, error) {
	start := time.Now()
	md, err := probe.GetMetadata(srcFilename)
	if err != nil {
		log.Error().Err(err).Msg("Failed to extract video metadata")
		return nil, err
	}

	since := time.Since(start)
	log.Info().Str("took", fmt.Sprintf("%v", since)).Str("vcodec", md.VideoCodec).Str("acodec", md.AudioCodec).Msg("Metadata extracted")

	props := md.ToProperties()

	if req.IsTest {
		fmt.Println(props.String())
	} else {
		err := updateAssetProps(req, props)
		if err != nil {
			return nil, err
		}
	}

	return md, nil
}

func updateAssetProps(req mimirapp.FileProcessorRequest, props mimir.Properties) error {
	start := time.Now()

	convertedProps, err := mimirpbconv.PropertiesToProtoBuf(props)
	if err != nil {
		req.Log.Error().Err(err).Msg("Failed to process asset properties")
		return err
	}

	ctx, cancel := context.WithTimeout(req.Command.Context(), 5*time.Second)
	defer cancel()

	_, err = req.Client.UpdateAssets(ctx, &mimirpb.UpdateAssetsRequest{
		Selector: &mimirpb.UpdateAssetsRequest_Ref{
			Ref: &mimirpb.AssetRef{
				Index:   req.Task.File().AssetRef().IndexName(),
				AssetId: req.Task.File().AssetRef().AssetID(),
			},
		},
		UpdateFiles: []*mimirpb.UpdateAssetFileRequest{
			{
				Selector: &mimirpb.UpdateAssetFileRequest_Ref{
					Ref: &mimirpb.FileRef{
						Repository: req.Task.File().FileRef().Repository,
						Filename:   req.Task.File().FileRef().Filename,
					},
				},
				SetProperties: convertedProps,
			},
		},
	})

	duration := time.Since(start)

	if err != nil {
		req.Log.Error().Err(err).Msg("Failed to update asset properties")
		return err
	}

	req.Log.Debug().Str("took", fmt.Sprintf("%v", duration)).Msg("Assets updated")
	return nil
}

func uploadAssetSidecars(req mimirapp.FileProcessorRequest, reqs []*mimirpb.UploadAssetSidecarRequest) error {
	start := time.Now()

	_, err := req.Client.UploadSidecars(context.Background(), &mimirpb.UploadSidecarsRequest{
		Asset: &mimirpb.AssetRef{
			Index:   req.Task.File().AssetRef().IndexName(),
			AssetId: req.Task.File().AssetRef().AssetID(),
		},
		Sidecars: reqs,
	})

	if err != nil {
		req.Log.Error().Err(err).Msg("Failed to upload sidecar")
		return err
	}

	duration := time.Since(start)

	sumBytes := 0
	for _, req := range reqs {
		sumBytes += len(req.Bytes)
	}

	req.Log.Info().Str("took", fmt.Sprintf("%v", duration)).Int("uploaded_bytes", sumBytes).Msg("Sidecar uploaded")
	return nil
}
