package thumbnailer

import (
	"bytes"
	"fmt"

	ffmpeg "github.com/u2takey/ffmpeg-go"
	mimir "gitlab.com/gomimir/processor"
)

type ThumbnailReq struct {
	Filename string
	Width    int
	Height   int
}

type Thumbnail struct {
	mimir.ThumbnailDescriptor
	Bytes []byte
}

func GetThumbnail(req ThumbnailReq) (*Thumbnail, error) {
	buf := bytes.NewBuffer(nil)
	err := ffmpeg.Input(req.Filename).
		Filter("select", ffmpeg.Args{fmt.Sprintf("gte(n,%d)", 5)}).
		Output("pipe:", ffmpeg.KwArgs{"vframes": 1, "format": "image2", "vcodec": "mjpeg"}).
		WithOutput(buf).
		Run()

	if err != nil {
		return nil, err
	}

	return &Thumbnail{
		ThumbnailDescriptor: mimir.ThumbnailDescriptor{
			Format: mimir.TF_JPEG,
			Width:  req.Width,
			Height: req.Height,
		},
		Bytes: buf.Bytes(),
	}, nil
}
