package transcoder

import (
	"bytes"

	"github.com/rs/zerolog/log"
	ffmpeg "github.com/u2takey/ffmpeg-go"
)

type TranscodeReq struct {
	InputFilename  string
	OutputFilename string

	VideoCodec string
	AudioCodec string
}

type TranscodeResult struct {
	Result *bytes.Buffer
}

func Transcode(req TranscodeReq) error {
	buf := bytes.NewBuffer(nil)
	errBuf := bytes.NewBuffer(nil)

	err := ffmpeg.Input(req.InputFilename).
		Output(req.OutputFilename, ffmpeg.KwArgs{"vcodec": req.VideoCodec, "acodec": req.AudioCodec}).
		WithOutput(buf).
		WithErrorOutput(errBuf).
		Run()

	if err != nil {
		log.Error().Err(err).Msgf("Failed to executed ffmpeg\n%v", errBuf)
		return err
	}

	return nil
}
