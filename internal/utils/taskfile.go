package utils

import (
	"fmt"
	"net/http"
	"os"
	"regexp"

	"github.com/rs/zerolog/log"
	mimirapp "gitlab.com/gomimir/processor/pkg/app"
)

var httpRX = regexp.MustCompile(`^https?:`)

type TaskFile struct {
	Filename     string
	NeedsCleanup bool
}

// downloadToTemp - downloads task file into temporary directory and returns its filename
func RetrieveTaskFile(req mimirapp.FileProcessorRequest) (*TaskFile, error) {
	var filename string
	if req.IsTest {
		filename = req.Task.File().FileRef().Filename
		if len(httpRX.FindStringSubmatch(filename)) > 0 {
			res, err := http.Get(filename)
			if err != nil {
				log.Error().Err(err).Msg("Failed to download file")
				return nil, err
			}

			defer res.Body.Close()

			if res.StatusCode != 200 {
				log.Error().Int("status", res.StatusCode).Msg("Failed to download file. Server responded with unexpected status code")
				return nil, fmt.Errorf("server responded with unexpected status code: %v", res.StatusCode)
			}

			filename, err = DownloadToTemp(req.Task.ID(), res.Body)
			if err != nil {
				log.Error().Err(err).Msg("Unable to download file")
				return nil, err
			}

			return &TaskFile{
				Filename:     filename,
				NeedsCleanup: true,
			}, nil

		} else {
			_, err := os.Stat(filename)
			if err != nil {
				req.Log.Error().Msg("File does not exist")
				return nil, err
			}

			return &TaskFile{
				Filename:     filename,
				NeedsCleanup: false,
			}, nil
		}
	} else {
		reader, err := req.Task.File().Reader()
		if err != nil {
			req.Log.Error().Err(err).Msg("Unable read file")
			return nil, err
		}

		defer reader.Close()

		filename, err = DownloadToTemp(req.Task.ID(), reader)
		if err != nil {
			log.Error().Err(err).Msg("Unable to download file")
			return nil, err
		}

		return &TaskFile{
			Filename:     filename,
			NeedsCleanup: true,
		}, nil
	}
}
