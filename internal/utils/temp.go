package utils

import (
	"fmt"
	"io"
	"os"
	"path"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
)

// Ensures that given path exists and it is a directory
func EnsureTempDir(temp string) {
	if temp == "" {
		log.Fatal().Msg("Missing temporary directory")
		os.Exit(1)
	}

	fi, err := os.Stat(temp)
	if err != nil {
		if os.IsNotExist(err) {
			log.Fatal().Str("path", temp).Msg("Temporary directory does not exist")
		} else {
			log.Fatal().Err(err).Msg("Unable to check temp directory")
		}
		os.Exit(1)
	}

	if !fi.IsDir() {
		log.Fatal().Str("path", temp).Msg("Invalid temp directory. Path is not a directory")
		os.Exit(1)
	}
}

// os.Remove wrapper with logging
func RemoveTempFile(filename string) {
	err := os.Remove(filename)
	if err != nil {
		log.Warn().Str("path", filename).Err(err).Msg("Failed to remove temporary file")
	}
}

func DownloadToTemp(filename string, reader io.Reader) (string, error) {
	tmpFilename := path.Join(viper.GetString("temp-dir"), filename)
	f, err := os.Create(tmpFilename)
	if err != nil {
		log.Error().Str("path", tmpFilename).Err(err).Msg("Unable to create temporary file")
		return "", err
	}

	start := time.Now()
	_, err = io.Copy(f, reader)
	if err != nil {
		log.Error().Str("path", filename).Err(err).Msg("Error while downloading file content")
		return "", err
	}

	since := time.Since(start)
	log.Info().Str("took", fmt.Sprintf("%v", since)).Msg("File downloaded")

	return tmpFilename, nil
}
