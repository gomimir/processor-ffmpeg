package utils

import (
	nativelog "log"
	"strings"

	"github.com/rs/zerolog/log"
)

type logWriter struct{}

func (lw *logWriter) Write(p []byte) (n int, err error) {
	// Output message to zerolog with Debug level
	log.Debug().Msg(strings.TrimRight(string(p), "\n"))
	return len(p), nil
}

// FFmpeg lib is using native logger without option to turn off the output
// We are stubbing the logger to not polute the app output
// (we expect JSON logger to be used in production)
func SetupLoggerStub() {
	// Do not output anything apart of message (no time)
	nativelog.SetFlags(0)

	// Use our stubbed log writer
	nativelog.SetOutput(&logWriter{})
}
