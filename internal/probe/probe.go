package probe

import (
	"encoding/json"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/rs/zerolog/log"

	ffmpeg "github.com/u2takey/ffmpeg-go"
	mimir "gitlab.com/gomimir/processor"
)

var fractionRX = regexp.MustCompile(`^([0-9]+)/([0-9]+)$`)
var gpsRX = regexp.MustCompile(`^([\+-]?[0-9]+(\.[0-9]+)?)([\+-]?[0-9]+(\.[0-9]+)?)`)

// https://developer.apple.com/library/archive/documentation/QuickTime/QTFF/Metadata/Metadata.html#//apple_ref/doc/uid/TP40000939-CH1-SW43
func GetMetadata(filename string) (*VideoMetadata, error) {
	probeStr, err := ffmpeg.ProbeWithTimeout(filename, 10*time.Second, ffmpeg.KwArgs{})
	if err != nil {
		return nil, err
	}

	// fmt.Println(probeStr)

	var res ffmpegResult
	err = json.Unmarshal([]byte(probeStr), &res)
	if err != nil {
		return nil, err
	}

	md := VideoMetadata{}

	if value := res.Format.Tags["com.apple.quicktime.creationdate"]; value != "" {
		t, err := time.Parse("2006-01-02T15:04:05Z0700", value)
		if err != nil {
			log.Warn().Err(err).Msg("Unable to parse com.apple.quicktime.creationdate")
		} else {
			md.CreationTime = t
		}
	}

	if md.CreationTime.IsZero() {
		if value := res.Format.Tags["creation_time"]; value != "" {
			t, err := time.Parse("2006-01-02T15:04:05.000000Z", value)
			if err != nil {
				log.Warn().Err(err).Msg("Unable to parse creation_time")
			} else {
				md.CreationTime = t
			}
		}
	}

	if value := res.Format.Tags["com.apple.quicktime.make"]; value != "" {
		md.CameraMake = value
	}

	if value := res.Format.Tags["com.apple.quicktime.model"]; value != "" {
		md.CameraModel = value
	}

	if res.Format.Duration != "" {
		dur, err := strconv.ParseFloat(res.Format.Duration, 64)
		if err != nil {
			log.Warn().Err(err).Msg("invalid duration")
		} else {
			md.DurationSecs = dur
		}
	}

	md.Type = "video"
	for tag := range res.Format.Tags {
		if strings.HasPrefix(tag, "com.apple.quicktime.live-photo") {
			md.Type = "live-photo"
			break
		}
	}

	if value := res.Format.Tags["com.apple.quicktime.location.ISO6709"]; value != "" {
		m := gpsRX.FindStringSubmatch(value)
		if len(m) == 5 {
			lat, err := strconv.ParseFloat(m[1], 64)
			if err == nil {
				long, err := strconv.ParseFloat(m[3], 64)
				if err == nil {
					md.GPS = &mimir.GPS{
						Latitude:  lat,
						Longitude: long,
					}
				}
			}
		}
	}

	if value := res.Format.Tags["com.apple.quicktime.content.identifier"]; value != "" {
		md.AppleContentIdentifier = value
	}

	for _, stream := range res.Streams {
		if stream.CodecType == "video" {
			if stream.CodecName != nil {
				md.VideoCodec = *stream.CodecName
			}

			if stream.FrameRate != nil {
				m := fractionRX.FindStringSubmatch(*stream.FrameRate)
				if len(m) == 3 {
					numerator, err := strconv.ParseInt(m[1], 10, 32)
					if err != nil {
						log.Warn().Err(err).Msg("invalid frame rate fraction, unable to parse numerator")
					} else {
						denominator, err := strconv.ParseInt(m[2], 10, 32)
						if err != nil {
							log.Warn().Err(err).Msg("invalid frame rate fraction, unable to parse denominator")
						} else {
							md.FrameRate = mimir.Fraction{
								Numerator:   numerator,
								Denominator: denominator,
							}
						}
					}
				} else {
					log.Warn().Err(err).Msg("invalid frame rate fraction")
				}

				if stream.Width != nil {
					md.Width = *stream.Width
				}

				if stream.Height != nil {
					md.Height = *stream.Height
				}
			}
		} else if stream.CodecType == "audio" {
			if stream.CodecName != nil {
				md.AudioCodec = *stream.CodecName
			}
		}
	}

	return &md, nil
}
