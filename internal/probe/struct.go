package probe

import (
	"time"

	mimir "gitlab.com/gomimir/processor"
)

type VideoMetadata struct {
	Type string

	AppleContentIdentifier string

	CreationTime time.Time

	CameraMake  string
	CameraModel string

	Width        int
	Height       int
	FrameRate    mimir.Fraction
	DurationSecs float64

	VideoCodec string
	AudioCodec string

	GPS *mimir.GPS
}

func (md VideoMetadata) ToProperties() mimir.Properties {
	props := mimir.Properties{}

	props.Set("type", md.Type)

	if md.AppleContentIdentifier != "" {
		props.Set("apple_contentId", md.AppleContentIdentifier)
	}

	if !md.CreationTime.IsZero() {
		props.Set("date", md.CreationTime)
		props.Set("authoredAt", md.CreationTime)
	}

	if md.Width != 0 {
		props.Set("image_width", md.Width)
	}

	if md.Height != 0 {
		props.Set("image_height", md.Height)
	}

	if md.DurationSecs != 0 {
		props.Set("video_duration", md.DurationSecs)
	}

	if md.FrameRate.Numerator != 0 || md.FrameRate.Denominator != 0 {
		props.Set("video_frameRate", md.FrameRate)
	}

	if md.VideoCodec != "" {
		props.Set("video_codec", md.VideoCodec)
	}

	if md.AudioCodec != "" {
		props.Set("audio_codec", md.AudioCodec)
	}

	if md.CameraMake != "" {
		props.Set("camera_make", md.CameraMake)
	}

	if md.CameraModel != "" {
		props.Set("camera_model", md.CameraModel)
	}

	if md.GPS != nil {
		props.Set("gps", *md.GPS)
	}

	return props
}

type ffmpegResult struct {
	Streams []ffmpegStream `json:"streams"`
	Format  ffmpegFormat   `json:"format"`
}

type ffmpegFormat struct {
	Filename string            `json:"filename"`
	Duration string            `json:"duration"`
	Tags     map[string]string `json:"tags"`
}

type ffmpegStream struct {
	Index     int    `json:"index"`
	CodecType string `json:"codec_type"`

	CodecName     *string `json:"codec_name"`
	CodecNameLong *string `json:"codec_long_name"`

	Width     *int    `json:"width"`
	Height    *int    `json:"height"`
	FrameRate *string `json:"r_frame_rate"`
}
