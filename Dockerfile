# Base image: debian + ffmpeg + ca-certificates
FROM debian:bullseye as base
RUN DEBIAN_FRONTEND=noninteractive apt-get update \
  && apt-get install -y --no-install-recommends \
      ca-certificates \
      ffmpeg \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

# Development image: base + golang
# Inspired by: https://github.com/docker-library/golang/blob/master/1.17/bullseye/Dockerfile
FROM base as dev
RUN DEBIAN_FRONTEND=noninteractive apt-get update \
  && apt-get install -y --no-install-recommends \
      curl \
      git \
      make

RUN set -eux; \
    URL=https://dl.google.com/go/go1.17.6.linux-amd64.tar.gz; \
    SHA256=231654bbf2dab3d86c1619ce799e77b03d96f9b50770297c8f4dff8836fc8ca2; \
    curl -o go.tar.gz $URL; \
    echo "$SHA256 *go.tar.gz" | sha256sum -c -; \
    tar -C /usr/local -xzf go.tar.gz; \
    rm go.tar.gz

ENV GOROOT /usr/local/go
ENV GOPATH /go
ENV PATH $GOPATH/bin:$GOROOT/bin:$PATH
RUN mkdir -p "$GOPATH/src" "$GOPATH/bin" && chmod -R 777 "$GOPATH"

RUN go get -v github.com/ramya-rao-a/go-outline golang.org/x/tools/gopls

# Build the app
FROM dev as build

# Download modules first to leverage layer caching
ADD go.mod /app/
ADD go.sum /app/
WORKDIR /app
RUN go mod download

# Build the app
ADD . /app
RUN make build

# Final image
FROM base as final
COPY --from=build /app/bin/mimir-processor-ffmpeg /mimir-processor-ffmpeg
CMD ["/mimir-processor-ffmpeg", "process"]
